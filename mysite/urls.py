from django.conf.urls import url
from django.contrib import admin

from blog.views import MainView, SearchView

urlpatterns = [
    url(r'^$', MainView.as_view(), name='main'),
    url(r'^search/$', SearchView.as_view(), name='search'),
    url(r'^admin/', admin.site.urls),
]
