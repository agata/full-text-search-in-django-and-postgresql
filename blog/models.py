from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.search import SearchVectorField


class Post(models.Model):
    class Language:
        english = 'eng'
        polish = 'pol'

        CHOICES = (
            (polish, _('Polish')),
            (english, _('English')),
        )

    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(
            default=timezone.now)
    language = models.CharField(
        max_length=3,
        choices=Language.CHOICES,
        default=Language.polish,
        verbose_name=_('Language'),
    )
    text_tsvector = SearchVectorField()

    def __str__(self):
        return self.title
