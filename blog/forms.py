from django import forms

from .models import Post


class SearchForm(forms.Form):
    search_phrase = forms.fields.CharField(
        label='Search for',
    )
    language = forms.ChoiceField(
        Post.Language.CHOICES,
        required=False,
    )
