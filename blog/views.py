from django.views.generic import ListView
from django.views.generic.edit import FormMixin
from django.contrib.postgres.search import SearchQuery

from .models import Post
from .forms import SearchForm


class MainView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'blog/main.html'


class SearchView(FormMixin, ListView):
    model = Post
    form_class = SearchForm
    template_name = 'blog/search.html'
    lang = {'pol': 'polish', 'eng': 'english'}

    def get_initial(self):
        initial = {}
        if self.request.GET.get('search_phrase'):
            initial['search_phrase'] = self.request.GET.get('search_phrase')
        return initial

    def get_queryset(self):
        search_phrase = self.request.GET.get('search_phrase')
        language = self.request.GET.get('language', Post.Language.polish)
        language = self.lang[language]

        if search_phrase:
            return Post.objects.filter(
                text_tsvector=SearchQuery(search_phrase, config=language)
            ).extra(
                select={"headline": "ts_headline(text, plainto_tsquery(%s), 'MaxWords = 30, MaxFragments = 4')"},
                select_params=[search_phrase]
            )

        return Post.objects.none()

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['search_phrase'] = self.request.GET.get('search_phrase')
        return context
